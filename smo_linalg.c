#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "smo_mem_alloc.h"
#include "smo_base.h"
#include "smo_linalg.h"


int LU_decomp( dmatrix_t* A, dmatrix_t* L, dmatrix_t* U, dmatrix_t* P)
{
    int test ;
    // test of the inputs
    if ( ( is_square(A) == 1) || ( is_square(L) == 1 ) || ( is_square(U) == 1 ) || ( is_square(P) == 1 )){ printf( " LU_decomp : one of the matrix is not square. \n") ; return -100 ; }
    if ( (compare_shape(A,L) == 1) || (compare_shape(L,U) == 1) || (compare_shape(U,P)==1) || (compare_shape(P,A)==1)){ printf( "LU_decomp : matrix doesn't have the same size. \n") ; return -100 ; }
    // end test of the inputs    

    const int n = A->nr ; // number of rows/columns we stock
    dmatrix_set_identity(L) ; // set the lower matrix to identity
    test = dmatrix_copy_inplace(A,U) ; // initialize the upper matrix to A
    if( test != 0){ printf( " LU_decomp : initialization problem of U. \n") ; return test ; }
    dmatrix_set_identity(P) ;

    for( int kc=0 ; kc<n-1 ; kc++ ) // loop on the columns (except the last one)
    {
	// first we're looking for the first row which as non-null value in the column
	int k_first_no_null = kc ; // to find the index of the first row which is non-null
	while(  (abs( dmatrix_get( U, k_first_no_null , kc ) ) < TOLERANCE ) && ( k_first_no_null < n-1 ) ){  k_first_no_null += 1 ; }
	// now we have in k_first_no_null the index of the line that we will use for pivot
	//if we are on the last row and that it's a zero, column is full of zero then nothing to do
	if ( (k_first_no_null == n-1 ) && (  abs(dmatrix_get( U, k_first_no_null , kc )) < TOLERANCE ) ) { ; }
	// else we swap the rows and do pivot
	else
	{
	    test = swap_rows_inplace( U, kc, k_first_no_null ) ;
	    if( test < 0 ){ fprintf( stderr, " error in LU_decomp in swap_rows_inplace. %i error \n ", test ) ; }	    
	    for( int kr=kc+1; kr<n ; kr++ ) // loop on the rows, from the kc+1 rows
	    {
		dmatrix_set( P, kc, kc, 0.0) ;
		dmatrix_set( P, k_first_no_null, k_first_no_null, 0.0) ;
		dmatrix_set( P, k_first_no_null, kc, 1.0) ;
		dmatrix_set( P, kc, k_first_no_null, 1.0) ;
		const double pivot_term = dmatrix_get(U, kr, kc) / dmatrix_get( U, kc, kc ) ;
		test = dmatrix_set( L, kr, kc, pivot_term ) ;
		if( test != 0){ printf(" LU_decomp : dmatrix_set error.\n" ) ; return test ; }
		for( int i = kc ; i<n ; i++)
		{
		    const double new_term = dmatrix_get( U, kr, i) -  pivot_term * dmatrix_get( U, kc, i ) ;
		    dmatrix_set( U, kr, i, new_term ) ;
		}
	    }
    	}
    }
    return 0 ;
}
    

int cholesky_decomp( dmatrix_t* A , dmatrix_t* L)
{
    printf( " /!\ WARNING /!\ WARNING /!\ WARNING /!\ \n" ) ;
    printf( " no symmetrical definite positive test is proceeded. Ensure that's theoretically the case \n") ;
    printf( " ------------- \n ") ;
    int test1 = is_square( A ) ;
    int test2 = compare_shape( A, L ) ;
    if( (test1==1) || (test2==1)){ printf( " dimension problem in cholesky_decomp \n") ; return -100 ; }
    const int n = A->nr ;
    dmatrix_set_zeros( L ) ;
    // first column
    const double l00 = sqrt( dmatrix_get( A, 0, 0) ) ;
    if (fabs( l00) < TOLERANCE ){ printf(" division by zero (or almost zero)  \n") ; return -300 ; }
    dmatrix_set( L, 0, 0, l00) ;
    for ( int kl = 1 ; kl<n ; kl++ )
    {
	const double a = dmatrix_get( A, 0, kl ) ;
	dmatrix_set( L, kl, 0, a/l00 ) ;
    }
    // other columns
    for (int kc = 1 ; kc<n ; kc++)
    {
	// computation of diagonal term
        double lik2 = 0 ;
        for ( int i = 0 ; i < kc ; i++ )
	{
	    const double a = dmatrix_get( L, kc, i ) ;
	    lik2 += a*a ;
	}
	if( lik2 > dmatrix_get( A, kc, kc) ){ printf(" negative square root \n") ; return -300 ; }
	const double diag_term = sqrt( dmatrix_get(A, kc, kc) - lik2 ) ;
	dmatrix_set( L, kc, kc, diag_term  ) ;
	// end of computation of diagonal term
        for (int kl = kc + 1 ; kl<n ; kl ++ )
        {            
            double ll = 0 ;
	    for( int i = 0 ; i < kc ; i++ )
	    {
		ll +=  dmatrix_get( L, kc, i) * dmatrix_get( L, kl, i ) ;
	    }
	    if( fabs(dmatrix_get(L,kc,kc)) < TOLERANCE){ printf(" division by zero (or almost zero) \n"); return -300 ;}
	    const double value_kl_kc = ( dmatrix_get( A, kl, kc) - ll )/dmatrix_get( L, kc, kc) ;
	    dmatrix_set( L, kl, kc, value_kl_kc ) ;
        }
    }
    return 0 ;
}




///////
///////
///////

/* int LU_decomp( double** A, int n, double** L, double** U ) */
/* { */
/*     set_zeros( L , n ) ; */
/*     set_zeros( L , n ); */
/*     for(int j=0; j<n; j++) */
/*     { */
/*         for(int i=0; i<n; i++) */
/*         { */
/*             if(i<=j) */
/*             { */
/*                 U[i][j]=A[i][j]; */
/*                 for(int k=0; k<i-1; k++) */
/*                     U[i][j]-=L[i][k]*U[k][j]; */
/*                 if(i==j) */
/*                     L[i][j]=1; */
/*                 else */
/*                     L[i][j]=0; */
/*             } */
/*             else */
/*             { */
/*                 L[i][j]=A[i][j]; */
/*                 for(int k=0; k<=j-1; k++) */
/*                     L[i][j]-=L[i][k]*U[k][j]; */
/* 		if ( fabs(U[j][j]) < 0.0000000001 ){ printf(" division par presque-zéro \n") ; return 1 ; } */
/*                 L[i][j]/=U[j][j]; */
/*                 U[i][j]=0; */
/*             } */
/*         } */
/*     } */
/*     return 0 ; */
/* } */








/* int solve_LXB( double** L, int n, double* b, double* sortie ) */
/* { */
/*     for( int i = 0 ; i < n ; i ++ ){ sortie[i] = 0.0 ; } */
/*     sortie[0] = b[0]/L[0][0] ; */
/*     for( int kl=1 ; kl<n ; kl++ ) */
/*     { */
/* 	double stmp = 0.0 ; */
/* 	for( int i = 0 ; i<kl ; i++){ stmp += sortie[i] * L[kl][i] ; } */
/* 	sortie[kl] = (b[kl] - stmp)/L[kl][kl] ; */
/*     } */
/*     return 0 ; */
/* } */

/* int solve_LUXB( double** L, int n, double* b, double* sortie ) */
/* { */
/*     for( int i = 0 ; i < n ; i ++ ){ sortie[i] = 0.0 ; } */
/*     sortie[n-1] = b[n-1]/L[n-1][n-1] ; */
/*     for( int kl=n-2 ; kl>=0 ; kl-- ) */
/*     { */
/* 	double stmp = 0.0 ; */
/* 	for( int i = n-1 ; i>=kl+1 ; i--){ stmp += sortie[i] * L[i][kl] ; } */
/* 	sortie[kl] = (b[kl] - stmp)/L[kl][kl] ; */
/*     } */
/*     return 0 ; */
/* } */




/* int inverse_MSP( double** A, int n, double** L, double** sortie) */
/* { */
/*     cholesky_decomp( A, n, L ) ; */
/*     double* utmp = (double*) malloc( n * sizeof(double) ) ; */
/*     double* vtmp = (double*) malloc( n * sizeof(double) ) ; */
/*     double* btmp = (double*) malloc( n * sizeof(double) ) ; */
/*     set_zeros( sortie, n ) ; */
/*     for ( int kc = 0 ; kc<n ; kc++) */
/*     { */
/* 	for ( int i = 0 ; i<n ; i++) */
/* 	{ */
/* 	    utmp[i] = 0.0 ; */
/* 	    vtmp[i] = 0.0 ; */
/* 	    btmp[i] = 0.0 ; */
/* 	} */
/* 	btmp[kc] = 1.0 ; */
/* 	solve_LXB( L, n, btmp, utmp ) ; */
/* 	solve_LUXB( L, n, utmp, vtmp ) ; */
/* 	for( int i = 0 ; i < n ; i++ ){ sortie[i][kc] = vtmp[i] ; } */
/*     } */
/*     free(utmp) ; */
/*     free(vtmp) ; */
/*     free(btmp) ; */
/*     return 0 ; */
/* } */
