#ifndef SMO_BASE_H
#define SMO_BASE_H

#include <stdio.h>
#include <stdlib.h>
#include "smo_mem_alloc.h"
#include <math.h>


// the precision used ( == 0 , to check if matrix is symmetric, etc. )
#define TOLERANCE 0.00000000001 

// the basic structure representating the matrix
// - nr is the number of rows
// - nc is the number of columns
// - data is a pointer containing the data
typedef struct{
    int nr ;
    int nc ;
    double* data ;
} dmatrix_t ;


// function to check if a given index (line number and column number) is in the given range of a matrix
// for example, if we have a 2x2 matrix (0,1) gives true (0) and (0,2) gives false (1)
// parameters :
// - dmat : a pointer to the matrix
// - ir : the index of the row we're checking
// - ic : the index of the column we're checking
// return : 0 if true, 1 if false
inline int check_index( dmatrix_t* dmat, int ir, int ic )
{
    const int nr = dmat->nr ;
    const int nc = dmat->nc ;
    if( ( ir>=nr ) || (ir<0) )  { return 1 ; }
    else if ( ( ic>=nc) || (ic<0 ) )  { return 1 ; }
    else { return 0 ; }
}
int compare_shape( dmatrix_t* dmat1, dmatrix_t* dmat2 ) ; // check if dmat1 and dmat2 have same shape : return 0 if True, 1 if False
int is_square( dmatrix_t* dmat ) ; // check if dmat is square :  return 0 if True, 1 if False
int is_symmetric( dmatrix_t* dmat) ; // check if dmat si symmetric (up to TOLERANCE precision) :  return 0 if True, 1 if False

dmatrix_t dmatrix_alloc_zeros( int nr, int nc ) ; // return a dmatrix of nr rows and nc columns filled with zeros
void dmatrix_free( dmatrix_t* dmat ) ; // free the dmatrix dmat (free dmat->data, set nr and nc to zeros
void dmatrix_set_zeros( dmatrix_t* dmat ) ; // set the matrix dmat to zero
int dmatrix_set_identity( dmatrix_t* dmat ) ; // if the matrix dmat is square, set it to the identity matrix. Return 0 if succesful, else return an error code (check the error code list for more information)
dmatrix_t dmatrix_copy( dmatrix_t* dmat ) ; // return a copy of the matrix dmat
int dmatrix_copy_inplace( dmatrix_t* dmati, dmatrix_t* dmato ) ; // copy the matrix input dmati in the output matrix dmato. Return 0 if succesful, else return an error code (check the error code list for more information)
dmatrix_t mult_alloc( dmatrix_t* dmat1, dmatrix_t* dmat2 ) ; // return a dmatrix with the number of rows of dmat1 and the number of colums of dmat2. Used for multiplication of matrix.

double dmatrix_get( dmatrix_t* dmat, int ir, int ic ) ; // return the element of the matrix dmat at the row ir and the column ic. If index are out of bounds, an exit(-200) is called
int dmatrix_set( dmatrix_t* dmat, int ir, int ic, double new_value ) ; // set the element of the matrix dmat at the row ir and the column ic to new_value.  If index are out of bounds, an exit(-200) is called. There is a int return at 0 if everything is everything went right. A bit weird, but I'm still thinking the way I'm dealing with the error.


int swap_rows_inplace( dmatrix_t* dmat, int ir1, int ir2 ) ; // swap the rows ir1 and ir2 of dmat. return 0 if everything went right, else an error code
int swap_columns_inplace( dmatrix_t* dmat, int ic1, int ic2 ) ; // swap the columns ir1 and ir2 of dmat. return 0 if everything went right, else an error code

int dmatrix_add( dmatrix_t* dmat1, dmatrix_t* dmat2, dmatrix_t* output ) ; // add matrix dmat1 and dmat2, stock the results in output. Return 0 if everything went right, else return an error code
int scalar_mult( dmatrix_t* dmat, double a, dmatrix_t* output) ; // multiply the matrix dmat by the scalar a. Stock the results in output. Return 0 if everything went right, else return an error code
int dmatrix_mult( dmatrix_t* dmat1, dmatrix_t* dmat2, dmatrix_t* output ) ; // multiply the matrix dmat1 by the matrix dmat2. Stock the results in output. Return 0 if everything went right, else return an error code

void dmatrix_print( dmatrix_t* dmat) ; // print the matrix dmat

#endif
