#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "smo_mem_alloc.h"
#include "smo_base.h"
#include "smo_linalg.h"
#include "unitary_test.h"

dmatrix_t sample_matrix()
{
    dmatrix_t A = dmatrix_alloc_zeros( 3, 3) ;
    dmatrix_set( &A, 0, 0, -2.0 ) ;
    dmatrix_set( &A, 0, 1, -1.0 ) ;
    dmatrix_set( &A, 1, 0, -1.0 ) ;
    dmatrix_set( &A, 1, 1, 2.0 ) ;
    dmatrix_set( &A, 1, 2, -1.0 ) ;
    dmatrix_set( &A, 2, 1, -1.0 ) ;
    dmatrix_set( &A, 2, 2, 2.0 ) ;
    return A ;
}

void alloc_test()
{
    printf(" ----------------------------- \n") ;
    printf(" TEST OF ALLOC, COPY... \n") ;
    printf(" ----------------------------- \n") ;

    int test = 1 ;
    dmatrix_t A = dmatrix_alloc_zeros(3,3) ;
    dmatrix_print(&A) ;
    dmatrix_free(&A) ;

    dmatrix_t A1 = dmatrix_alloc_zeros(3,3) ;
    test = dmatrix_set_identity( &A1 ) ;
    printf( " matrixs should be identity \n " );
    dmatrix_print(&A1) ;
    dmatrix_set_zeros( &A1 ) ;
    printf( " matrixs should be zero \n " );
    dmatrix_print(&A1) ;

    test = dmatrix_set_identity( &A1 ) ;
    dmatrix_t B = dmatrix_copy( &A1 ) ;
    printf(" both matrix should be equal \n") ;
    dmatrix_print(&B) ;
    dmatrix_print(&A1) ;
    test = dmatrix_copy_inplace( &A1, &B ) ;
    printf(" both matrix should be equal \n") ;
    dmatrix_print(&B) ;
    dmatrix_print(&A1) ;
    
    dmatrix_t B2 = dmatrix_alloc_zeros( 3.0 , 3 ) ;

    dmatrix_free(&A1) ;
    dmatrix_free(&B) ;
    dmatrix_free(&B2) ;
    printf(" ----------------------------- \n") ;
    printf(" END OF TEST OF ALLOC, COPY... \n") ;
    printf(" ----------------------------- \n") ;

}

void test_test()
{
    printf(" ----------------------------- \n") ;
    printf(" TEST OF TEST \n") ;
    printf(" ----------------------------- \n") ;

    int test = 1 ;
    dmatrix_t A = dmatrix_alloc_zeros(3,3) ;
    test = is_square(&A) ;
    printf(" A is square : %i \n" , test ) ;

    dmatrix_t B = dmatrix_alloc_zeros(3,2) ;
    test = is_square(&B) ;
    printf(" B is not square : %i \n" , test ) ;

    test = compare_shape( &A, &B ) ;
    printf( " A and B doesn't have the same shape :  %i \n" , test ) ;

    dmatrix_t C = dmatrix_alloc_zeros(3,3) ;
    test = compare_shape( &A, &C ) ;
    printf( " A and C have the same shape :  %i \n" , test ) ;

    test = dmatrix_set( &A , 0, 1, 1.0 );
    test = is_symmetric( &A ) ;
    printf( " A is not symmetric : %i \n" , test ) ;
    test = dmatrix_set( &A , 1, 0, 1.0 );
    test = is_symmetric( &A ) ;
    printf( " A is symmetric : %i \n" , test ) ;
    
    dmatrix_free(&A) ;
    dmatrix_free(&B) ;
    dmatrix_free(&C) ;
    printf(" ----------------------------- \n") ;
    printf(" END OF TEST OF TEST \n") ;
    printf(" ----------------------------- \n") ;

}

void get_set_test()
{

    printf(" ----------------------------- \n") ;
    printf(" TEST OF GET AND SET FUNCTIONS \n") ;
    printf(" ----------------------------- \n") ;
    int test = 1 ;
    dmatrix_t A = dmatrix_alloc_zeros(3,3) ;

    test = dmatrix_set( &A , 0, 0, 3.0 ) ;
    printf(" test0 = %i \n" , test ) ;
    
    double a = dmatrix_get( &A , 0 , 0 );
    printf( " a = %.2f \n" , a ) ;

    //a = dmatrix_get( &A , -1 , 0 ) ; // testing first negative index
    //a = dmatrix_get( &A , 0 , -1 ) ; // testing second negative index
    //a = dmatrix_get( &A , 4 , 0 ) ; // testing too big first index
    //a = dmatrix_get( &A , 0 , 4 ) ; // testing too big second index

    /* test = dmatrix_set( &A , -1 , 0, 2.0 ) ; // testing first negative index */
    /* printf(" test1 = %i \n" , test ) ; */
    /* test = dmatrix_set( &A , 0 , -1, 2.0 ) ; // testing second negative index */
    /* printf(" test2 = %i \n" , test ) ; */
    /* test = dmatrix_set( &A , 4 , 0, 2.0 ) ; // testing too big first index */
    /* printf(" test3 = %i \n" , test ) ; */
    /* test = dmatrix_set( &A , 0 , 4, 2.0 ) ; // testing too big second index */
    /* printf(" test4 = %i \n" , test ) ; */

    dmatrix_free(&A) ;
    printf(" ----------------------------- \n") ;
    printf(" END OF TEST OF GET AND SET \n") ;
    printf(" ----------------------------- \n") ;
    
}

void basic_algebric_operation_test()
{
    /* // add */
    /* test = dmatrix_add( &A1, &B, &A1 ) ; */
    
    /* // multiplication test */
    /* double a = 2.0 ; */
    /* test = scalar_mult( &A , a, &A ) ; */
	
    /* dmatrix_t AM = dmatrix_alloc_zeros(2,3) ; */
    /* dmatrix_t BM = dmatrix_alloc_zeros(3,6) ; */
    /* dmatrix_t CM = mult_alloc( &AM, &BM ) ; */
    /* test = dmatrix_mult( &AM, &BM, &CM ) ; */

    /* dmatrix_t AM2 = dmatrix_alloc_zeros(3,3) ; */
    /* dmatrix_t BM2 = dmatrix_alloc_zeros(2,2) ; */
    /* dmatrix_t CM2 = mult_alloc( &AM2, &BM2 ) ; */
    /* test = dmatrix_mult( &AM2, &BM2, &CM2 ) ; */
    /* printf(" %i \n", test ) ; */

}
