#ifndef UNITARY_TEST_H
#define UNITARY_TEST_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "smo_mem_alloc.h"
#include "smo_base.h"
#include "smo_linalg.h"
#include "unitary_test.h"

dmatrix_t sample_matrix() ;
void alloc_test() ;
void test_test() ;
void get_set_test() ;
void basic_algebric_operation_test() ;

#endif
