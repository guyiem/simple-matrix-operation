#ifndef SMO_LINALG_H
#define SMO_LINALG_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "smo_mem_alloc.h"
#include "smo_base.h"
#include "smo_linalg.h"

int cholesky_decomp( dmatrix_t* A , dmatrix_t* L) ; // cholesky decomposition of the matrix A, stocked in matrix L. WARNING : ACTUALLY NO TEST OF SYMMETRIC DEFINITE POSITIVE IS PROCEEDED, SO BE SURE THAT'S THEORITICALLY THE CASE.
int LU_decomp( dmatrix_t* A, dmatrix_t* L, dmatrix_t* U, dmatrix_t* P) ; // PLU decomposition of the matrix A.

#endif
