#include "smo_base.h"
#include "smo_mem_alloc.h"
#include <stdio.h>
#include <stdlib.h>

// les codes erreurs
// -100 : pb de dimension
// -200 : pb d'index
// -300 : math pb (zero division, neg square root, etc.)


////////////////////////////////
// ALLOCATION, FREE, COPY, ETC.
////////////////////////////////
dmatrix_t dmatrix_alloc_zeros( int nr, int nc )
{
    dmatrix_t output ;
    output.nr = nr ;
    output.nc = nc ;
    MEM_ALLOC( output.data, double, nr * nc ) ;
    for ( int i = 0 ; i< nr*nc ; i++ ){    output.data[i] = 0.0 ;   }
    return output ;
}


void dmatrix_free( dmatrix_t* dmat )
{
    dmat->nr = 0 ;
    dmat->nc = 0 ;
    MEM_FREE( dmat->data ) ;
}


void dmatrix_set_zeros( dmatrix_t* dmat )
{
    const int ne = dmat->nr * dmat->nc ;
    for ( int i = 0 ; i < ne ; i++ ){    dmat->data[i] = 0 ; }
}

int dmatrix_set_identity( dmatrix_t* dmat )
{
    if ( is_square(dmat) == 1 ){ printf(" dmatrix_set_identity : setting to identity a non-square matrix. \n" ) ; return -100 ;  }
    const int ne = dmat->nr * dmat->nc ;
    const int n = dmat->nr ;
    int test = 1 ;
    for ( int i = 0 ; i < ne ; i++ ){    dmat->data[i] = 0.0 ; }
    for ( int i = 0 ; i < n ; i++ )
    {   test = dmatrix_set( dmat, i, i, 1.0 ) ;
	if ( test == 1 ){ printf( " dmatrix_set_identity : dmatrix_set problem. \n"); return -200 ; }
    }
    return 0 ; 
}

dmatrix_t dmatrix_copy( dmatrix_t* dmat )
{
    dmatrix_t output ;
    output.nr = dmat->nr ;
    output.nc = dmat->nc ;
    const int ne = output.nr * output.nc ;
    MEM_ALLOC( output.data , double, ne ) ;
    for( int i = 0 ; i<ne ; i++){    output.data[i] = dmat->data[i] ; }
    return output ;
}

int dmatrix_copy_inplace( dmatrix_t* dmati, dmatrix_t* dmato )
{
    if ( compare_shape( dmati, dmato ) == 1 ){ printf( " dmatrix_copy_inplace : not the same shape. \n") ; return -100 ; }
    const int ne = dmati->nr * dmati->nc ;
    for( int i = 0 ; i < ne ; i++){ dmato->data[i] = dmati->data[i] ; }
    return 0 ;
}

dmatrix_t mult_alloc( dmatrix_t* dmat1, dmatrix_t* dmat2 )
{
    dmatrix_t output = dmatrix_alloc_zeros( dmat1->nr , dmat2->nc ) ;
    return output ;
}
//////////////////////////////////////
// END OF ALLOCATION, FREE, COPY, ETC.
/////////////////////////////////////


//////////////
// GET , SET
//////////////
double dmatrix_get( dmatrix_t* dmat, int ir, int ic )
{
    const int nc = dmat-> nc ;
    int test = check_index( dmat, ir, ic ) ;
    if (test==1){ fprintf( stderr, " index error in dmatrix_get \n") ; exit(-200) ; }
    return dmat->data[ ir*nc + ic ] ;    
}


int dmatrix_set( dmatrix_t* dmat, int ir, int ic, double new_value )
{
    const int nc = dmat-> nc ;
    int test = check_index( dmat, ir, ic ) ;
    if( test==1){  fprintf( stderr, " index error in dmatrix_set \n") ; exit(-200) ; return -200 ; } // I keep "return -200" even it's weird, because I'm still thinking on the way I'm dealing with error
    dmat->data[ ir*nc + ic ] = new_value  ;
    return 0 ;
}
///////////////////
// END OF GET, SET
///////////////////



//////////
// TEST
//////////
int compare_shape( dmatrix_t* dmat1 , dmatrix_t* dmat2 ) // check if two matrix have same shape
{
    if ( (dmat1->nr == dmat2->nr) && (dmat1->nc == dmat2->nc) ){ return 0 ; }
    else { return 1 ; }    
}


int is_square( dmatrix_t* dmat ) // check if a matrix is square
{
    if ( dmat->nr == dmat->nc){ return 0 ; }
    else{ return 1 ; }
}

int is_symmetric( dmatrix_t* dmat) // check if matrix is symmetric, to TOL precision
{
    int test = is_square( dmat ) ;
    if ( test == 1 ){ printf( " WARNING is_symmetric : matrix is not even square \n" ) ; return 1 ; }
    int N = dmat->nr ;
    for ( int kr = 1 ; kr<N ; kr++ )
    {
	for ( int kc=0; kc<kr ; kc++ )
	{
	    const double a = dmatrix_get( dmat, kr, kc ) ;
	    const double b = dmatrix_get( dmat, kc, kr ) ;
	    if ( abs(a-b)>TOLERANCE ){ return 1 ; }
	}
    }
    return 0 ;
}
///////////////
// END OF TEST
///////////////
    



//////////////////////////////////////
// BASIC ALGEBRIC OPERATION
//////////////////////////////////////
int dmatrix_add( dmatrix_t* dmat1, dmatrix_t* dmat2, dmatrix_t* output )
{
    // dimension compatibiliy test
    int test1 = compare_shape( dmat1, dmat2 ) ;
    int test2 = compare_shape( dmat1, output ) ;
    if ( (test1 == 1) || (test2==1) )
    {
	fprintf( stderr, " incompatible matrix dimensions in dmatrix_add \n") ;
	return -100 ;
    }
    // end of dimension compatibiliy test
    const int ne = dmat1-> nr * dmat1->nc ;
    for ( int i = 0 ; i<ne ; i++)
    {
	output->data[i] = dmat1->data[i] + dmat2->data[i] ;
    }
    return 0 ;
}

int scalar_mult( dmatrix_t* dmat, double a, dmatrix_t* output)
{
    int test = compare_shape( dmat, output ) ;
    if ( test == 1 )
    {
	fprintf( stderr, " incompatible matrix dimensions in scalar_mult \n") ;
	return -100 ;
    }
    const int ne = dmat->nr * dmat->nc ;
    for ( int i = 0 ; i<ne ; i++ )
    {
	output->data[i] = a * dmat->data[i] ;
    }
    return 0 ;
}

int dmatrix_mult( dmatrix_t* dmat1, dmatrix_t* dmat2, dmatrix_t* output )
{
    if ( dmat1->nc != dmat2-> nr )
    {
	fprintf( stderr , " incompatible matrix dimensions for input in dmatrix_mult \n " ) ;
	return -100 ;
    }
    if ( (output->nc != dmat2-> nc ) || (output->nr != dmat1->nr ) )
    {
	fprintf( stderr, " incompatible dimensions between dmatrix output and dmat1 and dmat2 in dmatrix_mult \n ") ;
    }
    double acc = 0 ;
    for ( int ic = 0 ; ic< dmat2->nc ; ic++)
    {
	for ( int ir=0 ; ir<dmat1->nr ; ir++ )
	{
	    acc = 0 ;
	    for ( int i = 0 ; i<dmat2->nr ; i++){ acc += dmatrix_get( dmat1, ir, i ) * dmatrix_get( dmat2, i, ic ) ; }
	    const int test = dmatrix_set( output, ir, ic, acc ) ;
	    if ( test != 0 )
	    {
		fprintf( stderr, " index error in dmatrix_mult \n") ;
		return -200 ;
	    }
	}
    }
    return 0 ;
}

//////////////////////////////////////
// END OF BASIC ALGEBRIC OPERATION
//////////////////////////////////////



////////////////////////////
// SWAP LINES AND COLUMNS
////////////////////////////
int swap_rows_inplace( dmatrix_t* dmat, int ir1, int ir2 )
{
    const int nr = dmat->nr ;
    const int nc = dmat->nc ;
    if ( (ir1<0) || (ir1 >= nr ) ){ fprintf( stderr , " line index ir1 out of range in swap_rows_inplace \n" ) ; return -200 ; }
    if ( (ir2<0) || (ir2 >= nr ) ){ fprintf( stderr , " line index ir2 out of range in swap__rows_inplace \n" ) ; return -200 ; }
    for( int ic = 0 ; ic<nc ; ic++)
    {
	const double value_tmp1 = dmatrix_get( dmat, ir1, ic ) ;
	const double value_tmp2 = dmatrix_get( dmat, ir2, ic ) ;
	dmatrix_set( dmat , ir1, ic, value_tmp2 ) ;
	dmatrix_set( dmat , ir2, ic, value_tmp1 ) ;
    }
    return 0 ;
}

int swap_columns_inplace( dmatrix_t* dmat, int ic1, int ic2 )
{
    const int nr = dmat->nr ;
    const int nc = dmat->nc ;
    if ( (ic1<0) || (ic1 >= nc ) ){ fprintf( stderr , " line index ic1 out of range in swap_columns_inplace \n" ) ; return -200 ; }
    if ( (ic2<0) || (ic2 >= nc ) ){ fprintf( stderr , " line index ic2 out of range in swap_columns_inplace \n" ) ; return -200 ; }
    for( int il = 0 ; il<nr ; il++)
    {
	const double value_tmp1 = dmatrix_get( dmat, il, ic1 ) ;
	const double value_tmp2 = dmatrix_get( dmat, il, ic2 ) ;
	dmatrix_set( dmat , il, ic1, value_tmp2 ) ;
	dmatrix_set( dmat , il, ic2, value_tmp1 ) ;
    }
    return 0 ;
}
//////////////////////////////
// END SWAP LINES AND COLUMNS
//////////////////////////////
    
////////////////
// PRINT
////////////////
void dmatrix_print( dmatrix_t* dmat)
{
    const int nr = dmat->nr ;
    const int nc = dmat->nc ;
    printf( " numbers of rows : %i \n" , nr ) ;
    printf( " numbers of cols : %i \n" , nc ) ;
    for ( int kr = 0 ; kr<nr ; kr++)
    {
	for ( int kc = 0 ; kc<nc ; kc++ )
	{
	    printf(" %.2f " , dmatrix_get( dmat, kr, kc ) ) ;
	}
	printf("\n") ;
    }
    printf("\n") ;
}


