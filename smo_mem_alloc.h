#ifndef MEM_ALLOC_H
#define MEM_ALLOC_H
#include <string.h>

//////////////////////////////////////
// MACRO FUNCTION TO DEAL WITH
// MEMORY ALLOCATION AND MEMORY FREE
//////////////////////////////////////

/**
*  \def MEM_ALLOC(_o,_t,_n)
*   Macro function allocating memory to its parameter _o of type _t and size _n
*/
#define MEM_ALLOC(_o,_t,_n) do { \
  if (!(_o=(_t *)malloc (sizeof (_t)*(_n)))) \
    fprintf (stderr, "ERROR memory allocation" \
             " impossible to allocate %s of type %s[%1d] in %s"\
             " line %1d\n", #_o, #_t, (_n), \
             __FILE__, __LINE__), abort (); \
  memset(_o, 0, sizeof(_t)*(_n));\
  } \
  while (0)


/**
* \def MEM_REALLOC(_o,_t,_n)
* Macro function reallocating memory to its parameter _o of type _t and size _n 
*/	     
#define MEM_REALLOC(_o,_t,_n) do { \
  if (!(_o=(_t *)realloc ((_o),sizeof (_t)*(_n)))) \
    fprintf (stderr, "ERROR memory re-allocation" \
             " impossible to reallocate %s of type %s[%1d] in %s"\
             " line %1d\n", #_o, #_t, (_n), \
             __FILE__, __LINE__), abort (); } while (0)

/**
* \def MEM_FREE(_o)
* Macro function freeing memory allocate to parameter (_o)
*/
#define MEM_FREE(_o)  do { free (_o); _o=NULL; } while(0)


#endif /* MEM_ALLOC_H */
